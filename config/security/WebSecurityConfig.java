package elitech.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	//private static final String[] PUBLIC_MATCHERS = { "/css/**", "/js/**", "/image/**", "/book/**", "/user/**" };

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.cors().and().csrf().disable().authorizeRequests();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		//  http.headers().httpStrictTransportSecurity().disable();
		http.authorizeRequests().antMatchers("/users/signin", "/").permitAll()//
				.antMatchers("/users/signup/*").permitAll()
				.antMatchers("/cours/").permitAll()
				.antMatchers("/cours/all").permitAll()
				.antMatchers("/cours/{id}").permitAll()
				.antMatchers("/cours/{id}/prerequis").permitAll()
				.antMatchers("/cours/{id}/update").permitAll()
				.antMatchers("/cours/{id}/delete").permitAll()
				.antMatchers("/cours/{id}/create").permitAll()
				.antMatchers("/parcours/all").permitAll()
				.antMatchers("/parcours/{id}/prerequis").permitAll()
				.antMatchers("/api/*").permitAll()
				.antMatchers("/users/signinmicrosoft/*").permitAll()
				.antMatchers("/users/updatePassword/*").permitAll()
				.antMatchers("/users/favoriscontactbyconnecteduser/*").permitAll()
				.antMatchers("/users/*").permitAll()
				.antMatchers("/users/getuser/*").permitAll()
				.antMatchers("/api/file/download/**").permitAll()
				.antMatchers("/api/entreprises/**").permitAll()
				.antMatchers("/api/offresAuto/create").permitAll()
				.antMatchers("/api/stageAuto/create").permitAll()
				.antMatchers("/api/offreauto/*").permitAll()
				.antMatchers("/api/stageautos/*").permitAll()
				.antMatchers("/api/pays/create").permitAll()
				.antMatchers("/api/pays/**").permitAll()
				.antMatchers("/api/villes/**").permitAll()
				.antMatchers("/api/file/**").permitAll()
				.antMatchers("/api/sendMail/**").permitAll()
				.antMatchers("/api/image/**").permitAll()
				.antMatchers("/api/search/**").permitAll()

				.antMatchers("/home/**").permitAll()
				.antMatchers("/home/*").permitAll()
				.antMatchers("/api/contacts/updatephoto/**").permitAll()

				.anyRequest().authenticated();

		http.exceptionHandling().accessDeniedPage("/login");
		http.apply(new JwtTokenFilterConfigurer(jwtTokenProvider));

	}

	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/*.ico").and();
		web.ignoring().antMatchers("/offresAuto/create").and();
		web.ignoring().antMatchers("/stageAuto/create").and();
		web.ignoring().antMatchers("/api/stageautos/*").and();
		web.ignoring().antMatchers("/api/offreauto/*").and();
		web.ignoring().antMatchers("/api/sendMail/**").and();
		web.ignoring().antMatchers("/assets/**").and();
		web.ignoring().antMatchers("/static/**").and();
		 web.ignoring().antMatchers("/api/villes/**").and();
		 web.ignoring().antMatchers("/api/file/**").and();
		 web.ignoring().antMatchers("/api/pays/create").and();
		 web.ignoring().antMatchers("/api/pays/**").and();
		web.ignoring().antMatchers("/*.eot").and();
		web.ignoring().antMatchers("/assets/layout/images/pages/UnderConstruction.jpg").and();
		web.ignoring().antMatchers("/assets/layout/images/educationlogo.png").and();
      web.ignoring().antMatchers("/assets/layout/images/pages/icon1.png").and();
   web.ignoring().antMatchers("/assets/layout/images/pages/icon.png").and();
   web.ignoring().antMatchers("/assets/layout/images/pages/logoelitech.png").and();
		web.ignoring().antMatchers("/assets/layout/images/avatar.png").and();
   web.ignoring().antMatchers("/assets/layout/images/logoelitech.png").and();
    web.ignoring().antMatchers("/assets/layout/images/logoEstya.png").and();
		web.ignoring().antMatchers("/assets/layout/images/educationlogo.png").and();

		web.ignoring().antMatchers("/*.svg").and();
		web.ignoring().antMatchers("/*.ttf").and();
		web.ignoring().antMatchers("/*.woff").and();
		web.ignoring().antMatchers("/*.woff2").and();
		web.ignoring().antMatchers("/*.js").and();
		web.ignoring().antMatchers("/*.css").and();
		web.ignoring().antMatchers("/*.map").and();
		web.ignoring().antMatchers("/*.png").and();
		web.ignoring().antMatchers("/*.jpg").and();
		web.ignoring().antMatchers("/v2/api-docs").antMatchers("/swagger-resources/**").antMatchers("/swagger-ui.html")
				.and().ignoring().antMatchers("/h2-console/**/**");
		;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12);
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

}
