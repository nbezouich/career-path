package elitech.repository;

import elitech.model.Cours;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface CoursRepository extends Neo4jRepository<Cours,Long> {


    @Query("WITH 'http://dev-training.elitech.fr/wp-json/wplms/v1/course' AS url\n" +
            "CALL apoc.load.json(url)\n" +
            "yield value\n" +
            "UNWIND value.data as d\n" +
            "MERGE(cours:Cours{id:d.id})\n" +
            "ON CREATE SET cours.title = d.name,\n" +
            "\t\t\t  cours.link=url+'/'+d.id\n" +
            "WITH cours.link AS url2\n" +
            "CALL apoc.load.json(url2) YIELD value\n" +
            "UNWIND value.data AS d1\n" +
            "Match (n:Cours)\n" +
            "WHERE n.id=d1.course.id \n" +
            "SET n.description=d1.description\n" +
            "WITH * WHERE NOT d1.curriculum=FALSE \n" +
            "FOREACH (chap IN d1.curriculum | MERGE (chapitre:Chapitre{title:chap.title}) MERGE(chapitre)-[:APPARTIENT]->(n))")//requete comme celle que j'ai faite sur neo4j pour tous les cours
    void saveAll();






}
