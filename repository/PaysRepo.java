package elitech.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import elitech.model.Pays;

public interface PaysRepo extends CrudRepository<Pays, Long> {

	Optional<Pays> findByLibelle(String libelle);

}
