package elitech.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import elitech.model.Pays;
import elitech.model.Ville;

import java.util.List;
import java.util.Optional;

public interface VilleRepo extends JpaRepository<Ville, Long> {
	Optional<Ville> findByLibelleAndEquivalents( String libelle , String Equivalents );
	Ville findFirstByPaysAndLibelleContainingIgnoreCase(Pays pays, String libelle);
	Optional<Ville> findByLibelleAndCode(String libelle, String code);
	Ville findFirstByLibelle( String libelle);
	Ville findFirstByCode(String code);
	Ville findFirstByLibelleOrEquivalentsContainsIgnoreCase(String libelle, String equivalents);
	List<Ville> findByPays(Pays pays);
	List<Ville> findByLibelleOrEquivalents( String libelle , String Equivalents );
}

