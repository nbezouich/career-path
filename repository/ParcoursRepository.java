package elitech.repository;

import elitech.model.Parcours;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface ParcoursRepository extends Neo4jRepository<Parcours,Long> {

    @Query("Create (a: Parcours{title:\"Data Analyst\"})," +
            "(b: Parcours{title:\"Data Visualisation Analayst\"})"+
            "(c: Parcours{title:\"Data Analyst Expert\"})," +
            "(d: Parcours{title:\"Data Scientist\"})," +
            "(e: Parcours{title:\"Data Scientist Expert\"})," +
            "(f: Parcours{title:\"Data Engineer with Python\"})," )
     void saveParcours();

    @Query("Match(n:Parcours{})")//Query match les parcours avec les cours prerequis
     void saveRelationship();
}
