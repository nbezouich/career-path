package elitech.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import elitech.model.Contact;
import elitech.model.Entreprise;
import elitech.model.User;

public interface ContactRepo extends JpaRepository<Contact, Long> {



	@Query("SELECT u FROM Contact u  WHERE u.role='1' OR u.role='5' OR u.role='6' order by u.createdDate desc ")
	List<Contact> findCommercial();


	@Transactional
	void deleteByEmail(String email);

	Optional<Contact> findByEmail(String email);

	List<Contact> findByEntreprise(Entreprise entreprise);

	// List<Candidat> getListCandidatMatchingByPartenaire(@Param("idPartenaire")
	// long idPartenaire);

	// List<Etudiant> getListEtudiantMatchingByPartenaire(@Param("idPartenaire")
	// long idPartenaire);


	//@Query("SELECT u FROM Contact u" + " WHERE u.entreprise = :entreprise  " + " AND u.role='3' OR u.role='7' OR u.role='8'")
	List<Contact> findContactByEntreprise(Entreprise entreprise);

	Contact findContactByUser(User user);

}