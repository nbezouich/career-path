package elitech.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import elitech.model.Entreprise;

public interface EntrepriseRepo extends JpaRepository<Entreprise, Long> {

	Optional<Entreprise> findByLibelle(String libelle);

	@Query("SELECT u FROM Entreprise u WHERE u.libelle LIKE 'Elite%' ")
	List<Entreprise> findEntrepriseElitech();

}
