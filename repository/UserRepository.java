package elitech.repository;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import elitech.model.Contact;
import elitech.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	boolean existsByEmail(String email);

	Optional<User> findByEmail(String email);

	@Transactional
	void deleteByEmail(String email);

	@Query("SELECT u.listContact FROM User u " + "WHERE u.id = :id ")
	List<Contact> ListContactFavorisByConnectedUser(@Param("id") long id);

}