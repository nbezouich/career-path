package elitech.dto;

import java.io.Serializable;

public class ContactDto implements Serializable {
    private Long id;
    private String email;
    private  String nom;
    private String prenom;
    private Long totalProspect;
    private Long totalmatching;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Long getTotalProspect() {
        return totalProspect;
    }

    public void setTotalProspect(Long totalProspect) {
        this.totalProspect = totalProspect;
    }

    public Long getTotalmatching() {
        return totalmatching;
    }

    public void setTotalmatching(Long totalmatching) {
        this.totalmatching = totalmatching;
    }
}
