package elitech.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import elitech.exception.CustomException;
import elitech.model.Entreprise;
import elitech.repository.EntrepriseRepo;

@Service
public class EntrepriseServiceImpl implements EntrepriseService {
	
	@Autowired
	EntrepriseRepo repository;

	@Override
	public List<Entreprise> getAll() {
		try {
			//System.out.println("Get all Entreprises...");
			List<Entreprise> Entreprise = repository.findAll(Sort.by(Sort.Direction.DESC, "date"));
			return Entreprise;
		} catch (Exception e) {
			System.err.println("catch Entreprise 1");
			System.err.println(e.toString());
			System.err.println("catch Entreprise 1");
			return null;
		}
	}

	@Override
	public Entreprise save(Entreprise Entreprise) {
		try {
			Optional<Entreprise> _entreprise = repository.findByLibelle(Entreprise.getLibelle());
			if (!_entreprise.isPresent()) {
				System.out.println("begin create Entreprise");
				System.out.println(Entreprise.getCreatedBy());
				Entreprise _Libelle = repository.save(new Entreprise(Entreprise.getLibelle() , Entreprise.getDate() ,Entreprise.getCreatedBy(),
						Entreprise.getIdUnique(), Entreprise.getAdresse(), Entreprise.getTel() , Entreprise.getDescription(), Entreprise.getEmail(),Entreprise.getLien()));
				_Libelle.setIdUnique(_Libelle.generateIdUnique());
				repository.save(_Libelle);
				return _Libelle;
			} else
				throw new CustomException("Entreprise existe déja", HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			throw new CustomException("Entreprise existe déja", HttpStatus.UNPROCESSABLE_ENTITY);
		}

	}

	@Override
	public Optional<Entreprise> findByLibelle(String libelle) {
		return repository.findByLibelle(libelle);
	}

	@Override
	public List<Entreprise> getListElitech(){
		return repository.findEntrepriseElitech();
	}

	@Override
	public Optional<Entreprise> findById(long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}

	@Override
	public Entreprise update(long id, Entreprise entreprise) {
		try {
			System.out.println("Update user with ID..." + id);
			Optional<Entreprise> entreprise1 = repository.findById(id);
			if (entreprise1.isPresent()) {
				Entreprise _entreprise = entreprise1.get();
				_entreprise.setLibelle(entreprise.getLibelle());
				_entreprise.setAdresse(entreprise.getAdresse());
				_entreprise.setTel(entreprise.getTel());
				_entreprise.setDescription(entreprise.getDescription());
				_entreprise.setLien(entreprise.getLien());
				_entreprise.setEmail(entreprise.getEmail());
				return repository.save( _entreprise);
			} else {
				return null;
			}
		} catch (Exception e) {
			System.err.println(e.toString());
			return null;
		}
	}
}
