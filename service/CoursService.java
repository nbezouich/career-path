package elitech.service;

import elitech.model.Cours;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

public interface CoursService {

    List<Cours> getAll();
    Optional<Cours> findById(long id);
    Cours update(long id, Cours cours) ;
    Cours postCours(Cours _cours, long id);
    void deleteById(long id);
    void deleteAll();
}