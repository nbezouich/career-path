package elitech.service;

import java.util.List;
import java.util.Optional;
import elitech.model.Entreprise;

public interface EntrepriseService {
	
	List<Entreprise> getAll();

	Entreprise save(Entreprise entreprise);

	Optional<Entreprise> findByLibelle(String libelle);
	
	List<Entreprise> getListElitech();
	
	Optional<Entreprise> findById(long id);
	
	Entreprise update(long id, Entreprise entreprise) ;

}
