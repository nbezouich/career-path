package elitech.service;

import java.sql.Blob;
import java.util.List;
import java.util.Optional;

import elitech.dto.ContactDto;
import org.springframework.data.repository.query.Param;

import elitech.model.Contact;
import elitech.model.Entreprise;


public interface ContactService {

	Optional<Contact> getcontactById(Long id);

	Contact postCandidatUT(Contact contact, long id);

	Contact postPartenaireUT(Contact contact, long id);

	Contact postCommercialUT(Contact contact, long id);


	Contact updateUser(long id, Contact user);

	List<Contact> findCommercial();

	Optional<Contact> findByEmail(String email);
	
	Contact save(Contact contact);
	
	 Contact postEtudiantUT(Contact contact,long id);

	 Contact postPartenaire(Contact contact, long id);
	 
	Contact postAdminUT(Contact contact, long id);
	
	Optional<Contact> findById(long id);
	
	List<Contact> findContactByEntreprise(Entreprise entreprise);

	Contact saveCommercialFavorisStage(Contact contact);

	Contact saveCommercialFavorisEtudiant(Contact contact);
	
	Contact updateNote(long id, String note) ;
	
	Contact update(long id, Contact contact) ;
	
	 Contact updateContactEtudiant(Contact contact);


	Contact postPartenaireEducation(Contact contact, long id) ;
	
	Contact postPartenaireConsulting(Contact contact, long id);
	
	Contact postCommercialEducation(Contact contact, long id);
	
	Contact postCommercialConsulting(Contact contact, long id);

	Contact updateStatus(long id, Contact contact);






}
