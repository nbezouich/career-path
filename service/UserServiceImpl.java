package elitech.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import elitech.config.security.JwtTokenProvider;
import elitech.exception.CustomException;
import elitech.model.Contact;
import elitech.model.User;
import elitech.repository.ContactRepo;
import elitech.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private ContactService contactService;

	@Autowired
	private ContactRepo contactRepository;

	@Override
	public String signin(String email, String password) {
		try {
			User userData = null;
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
			Optional<User> user = userRepository.findByEmail(email);
			if (user.isPresent()) {
				userData = user.get();
				// System.out.print(userData.isEtat());
			}
			return jwtTokenProvider.createToken(email, userData.getRole(), userData.isEtat());
		} catch (AuthenticationException e) {
			throw new CustomException("Email / Mot de passe incorrect", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	@Override
	public String signinmicro(String email) {
		try {
			User userData = null;
			Optional<User> user = userRepository.findByEmail(email);
			if (user.isPresent()) {
				userData = user.get();
				// System.out.print(userData.isEtat());
			}
			return jwtTokenProvider.createToken(email, userData.getRole(), userData.isEtat());
		} catch (AuthenticationException e) {
			throw new CustomException("Email incorrect", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public String signup(User user) {
		if (!userRepository.existsByEmail(user.getEmail())) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			userRepository.save(user);
			// User _user=userRepository.save(new
			// User(user.getUsername(),user.getPassword(),user.getRoles()));
			// user.getContact().setCode("commercial");
			/*
			 * contactRepo.save(new Contact(user.getContact().getNom(),
			 * user.getContact().getPrenom(), user.getContact().getEmail(),
			 * user.getContact().getTelephone(), user.getContact().getEntreprise(),
			 * user.getContact().getCode(), user.getContact().getPays(),
			 * user.getContact().getVille(), user.getContact().getAdresse(),
			 * user.getContact().getFonction(), user.getContact().getNotes(),_user));
			 */
			return jwtTokenProvider.createToken(user.getEmail(), user.getRole(), user.isEtat());
		} else {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public Contact signupWithContact(Contact contact, long id) {
		Contact Contact = new Contact();
		if (!userRepository.existsByEmail(contact.getUser().getEmail())) {
			contact.getUser().setPassword(passwordEncoder.encode(contact.getUser().getPassword()));
			User user = userRepository.save(contact.getUser());
			contact.setUser(user);
			System.out.print(contact.getUser());
			if (contact.getRole().getAuthority() == "ROLE_COMMERCIAL") {
			Contact = 	contactService.postCommercialUT(contact, id);
			} else if (contact.getRole().getAuthority() == "ROLE_CANDIDAT") {
				System.out.print(contact.getUser());
				Contact = 	contactService.postCandidatUT(contact, id);
			} else if (contact.getRole().getAuthority() == "ROLE_ETUDIANT") {
				System.out.print(contact.getUser());
				Contact = 	contactService.postEtudiantUT(contact, id);
			} else if (contact.getRole().getAuthority() == "ROLE_ADMIN") {
				System.out.print(contact.getUser());
				Contact = 	contactService.postAdminUT(contact, id);
			} else if (contact.getRole().getAuthority() == "ROLE_PARTENAIRE") {
				System.out.print(contact.getUser());
				Contact = 	contactService.postPartenaire(contact, id);
			} else if (contact.getRole().getAuthority() == "ROLE_COMMERCIAL_EDUCATION") {
				Contact = 	contactService.postCommercialEducation(contact, id);
			} else if (contact.getRole().getAuthority() == "ROLE_COMMERCIAL_CONSULTING") {
				Contact = 	contactService.postCommercialConsulting(contact, id);
		    } else if (contact.getRole().getAuthority() == "ROLE_PARTENAIRE_EDUCATION") {
		    	Contact = 	contactService.postPartenaireEducation(contact, id);
			} else if (contact.getRole().getAuthority() == "ROLE_PARTENAIRE_CONSULTING") {
				Contact = 	contactService.postPartenaireConsulting(contact, id);
			}
         return Contact ;
			//return "Contact a été ajouté";
		} else {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public User search(String email) {
		User userData = null;
		Optional<User> user = userRepository.findByEmail(email);
		if (user.isPresent()) {
			userData = user.get();
			return userData;
		} else {
			throw new CustomException("L'utilisateur n'existe pas", HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public User findByUsername(HttpServletRequest req) {
		User userData = null;
		Optional<User> user = userRepository
				.findByEmail(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
		if (user.isPresent()) {
			userData = user.get();
			return userData;
		} else {
			throw new CustomException("L'utilisateur n'existe pas", HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public String refresh(String email) {
		User userData = null;
		Optional<User> user = userRepository.findByEmail(email);
		if (user.isPresent()) {
			userData = user.get();
		}
		return jwtTokenProvider.createToken(email, userData.getRole(), userData.isEtat());
	}

	@Override
	public String update(User user) {
		Optional<User> UserData = userRepository.findByEmail(user.getEmail());
		if (UserData.isPresent()) {
			User _user = UserData.get();
			// _user.setContacts(user.getContacts());
			// System.out.println("list contact user service "+ _user.getContacts().size());
			// _user.setPassword(passwordEncoder.encode(user.getPassword()));
			userRepository.saveAndFlush(_user);
			return "Mot de passe a été modifié";
		} else {
			throw new CustomException("Erreur", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public String delete(String email) {
		Optional<Contact> contact = contactRepository.findByEmail(email);
		Optional<User> user = userRepository.findByEmail(email);
		if (user.isPresent() && contact.isPresent()) {
			contactRepository.deleteByEmail(email);
			userRepository.deleteByEmail(email);
			return "Contact a été supprimé";
		}

		else {
			throw new CustomException("Erreur", HttpStatus.UNPROCESSABLE_ENTITY);
		}

	}

	@Override
	public Optional<User> findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public String ChangeEtat(String email) {
		Optional<User> UserData = userRepository.findByEmail(email);
		if (UserData.isPresent()) {
			User _user = UserData.get();
			System.out.print(_user.isEtat());
			if (_user.isEtat() == true) {
				_user.setEtat(false);
				userRepository.save(_user);
				return "Utilisateur a été bloqué";
			} else if (_user.isEtat() == false) {
				_user.setEtat(true);
				userRepository.save(_user);
				return "Utilisateur a été débloqué";
			} else {
				throw new CustomException("Erreur", HttpStatus.UNPROCESSABLE_ENTITY);
			}
		} else {
			throw new CustomException("Erreur", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public String updatePassword(User user) {
		Optional<User> UserData = userRepository.findByEmail(user.getEmail());
		if (UserData.isPresent()) {
			User _user = UserData.get();
			_user.setPassword(passwordEncoder.encode(user.getPassword()));
			userRepository.saveAndFlush(_user);
			return "Mot de passe a été modifié";
		} else {
			throw new CustomException("Erreur", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public List<Contact> ListContactFavorisByConnectedUser(long id) {

		return userRepository.ListContactFavorisByConnectedUser(id);
	}

	@Override
	public User updatefavoris(User user) {

		userRepository.saveAndFlush(user);
		return user;

	}

}
