package elitech.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import elitech.model.Pays;
import elitech.model.Ville;
import elitech.repository.PaysRepo;
import elitech.repository.VilleRepo;

@Service
public class PaysServiceImpl implements PaysService {

	@Autowired
	PaysRepo repository;
	
	@Autowired
	VilleRepo villerepo;

	@Override
	public List<Pays> getAll() {
		try {
			//System.out.println("Get all Pays...");
			List<Pays> Pays = new ArrayList<>();
			repository.findAll().forEach(Pays::add);
			return Pays;
		} catch (Exception e) {
			System.err.println("catch Pays 1");
			System.err.println(e.toString());
			System.err.println("catch Pays 1");
			return null;
		}
	}
	
	@Override
	public Ville save(String pays , String ville , String etat) {
		Optional<Pays> _pays = repository.findByLibelle(pays);
		Optional<Ville> _ville= villerepo.findByLibelleAndEquivalents(ville, etat);
		if (!_pays.isPresent() && !_ville.isPresent() ) {
			Pays newpays= new Pays(pays,pays);
			Pays savedpays = repository.save(newpays);
			Ville newVille= new Ville(ville,ville,etat,savedpays);
			Ville savedville = villerepo.save(newVille);
			String msg = "Pays et ville ont eté ajouté avec succès" ;
			return savedville;
		}
		else if(_pays.isPresent()  && !_ville.isPresent()) {
			Ville newVille= new Ville(ville,ville,etat,_pays.get());
			Ville savedville = villerepo.save(newVille);
			String msg = "Ville a eté ajouté avec succè" ;
			return savedville;
		} else return _ville.get();
		
	}

	@Override
	public  Optional<Pays> findbyLibelle(String libelle) {
		return  repository.findByLibelle(libelle);
	}

}
