package elitech.service;

import java.util.List;

import elitech.model.Pays;
import elitech.model.Ville;

public interface VilleService {

	public List<Ville> getAll();
	 List<Ville> getVilleByPays(long id);
	

}
