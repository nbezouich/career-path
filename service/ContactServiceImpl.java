package elitech.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import elitech.dto.ContactDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import elitech.exception.CustomException;
import elitech.model.Contact;
import elitech.model.Entreprise;
import elitech.model.Role;
import elitech.repository.ContactRepo;
import elitech.repository.UserRepository;

@Service
public class ContactServiceImpl implements ContactService {

	private static final Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);

	@Autowired
	private ContactRepo contactRepo;
	@Autowired
private UserRepository userepo;

	@Override
	public Optional<Contact> getcontactById(Long id) {
		return contactRepo.findById(id);

	}

	@Override
	public Contact postCandidatUT(Contact contact, long id) {
		try {
			if (id != 0) {
				Contact Contact = contactRepo.findById(id).get();
				contact.setRole(Role.ROLE_CANDIDAT);
				Contact _contact = contactRepo
						.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
								contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
								contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes(),
								contact.getUser(), contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom())));
				return _contact;
			} else {
				contact.setRole(Role.ROLE_CANDIDAT);
				Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(),
						contact.getEmail(), contact.getTelephone(), contact.getEntreprise(), contact.getRole(),
						contact.getPays(), contact.getVille(), contact.getAdresse(), contact.getFonction(),
						contact.getNotes(), contact.getUser(), "null"));
				return _contact;

			}
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public Contact postEtudiantUT(Contact contact, long id) {
		try {
			if (id != 0) {
				Contact Contact = contactRepo.findById(id).get();
				if (Contact.getRole() != Role.ROLE_ETUDIANT) {
					contact.setRole(Role.ROLE_ETUDIANT);
					Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(),
							contact.getEmail(), contact.getTelephone(), contact.getEntreprise(), contact.getRole(),
							contact.getPays(), contact.getVille(), contact.getAdresse(), contact.getFonction(),
							contact.getNotes(), contact.getUser(),
							contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom()), id));
					return _contact;
				} else {
					contact.setRole(Role.ROLE_ETUDIANT);
					Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(),
							contact.getEmail(), contact.getTelephone(), contact.getEntreprise(), contact.getRole(),
							contact.getPays(), contact.getVille(), contact.getAdresse(), contact.getFonction(),
							contact.getNotes(), contact.getUser(), "null",  null));
					return _contact;
				}
			} else {
				contact.setRole(Role.ROLE_ETUDIANT);
				Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(),
						contact.getEmail(), contact.getTelephone(), contact.getEntreprise(), contact.getRole(),
						contact.getPays(), contact.getVille(), contact.getAdresse(), contact.getFonction(),
						contact.getNotes(), contact.getUser(), "null", null));
				return _contact;

			}
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public Contact postAdminUT(Contact contact, long id) {
		try {
			Contact Contact = contactRepo.findById(id).get();
			contact.setRole(Role.ROLE_ADMIN);
			Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
					contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
					contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes(),
					contact.getUser(), contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom())));
			return _contact;
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public Contact postPartenaire(Contact contact, long id) {
		try {
			if (id != 0) {

				Contact Contact = contactRepo.findById(id).get();
				contact.setRole(Role.ROLE_PARTENAIRE);
				Contact _contact = contactRepo
						.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
								contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
								contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes(),
								contact.getUser(), contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom())));
				return _contact;
			} else {
				contact.setRole(Role.ROLE_PARTENAIRE);
				Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(),
						contact.getEmail(), contact.getTelephone(), contact.getEntreprise(), contact.getRole(),
						contact.getPays(), contact.getVille(), contact.getAdresse(), contact.getFonction(),
						contact.getNotes(), contact.getUser(), "null"));
				return _contact;

			}
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public Contact postPartenaireEducation(Contact contact, long id) {
		try {
			if (id != 0) {

				Contact Contact = contactRepo.findById(id).get();
				contact.setRole(Role.ROLE_PARTENAIRE_EDUCATION);
				Contact _contact = contactRepo
						.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
								contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
								contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes(),
								contact.getUser(), contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom())));
				return _contact;
			} else {
				contact.setRole(Role.ROLE_PARTENAIRE_EDUCATION);
				Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(),
						contact.getEmail(), contact.getTelephone(), contact.getEntreprise(), contact.getRole(),
						contact.getPays(), contact.getVille(), contact.getAdresse(), contact.getFonction(),
						contact.getNotes(), contact.getUser(), "null"));
				return _contact;

			}
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public Contact postPartenaireConsulting(Contact contact, long id) {
		try {
			if (id != 0) {

				Contact Contact = contactRepo.findById(id).get();
				contact.setRole(Role.ROLE_PARTENAIRE_CONSULTING);
				Contact _contact = contactRepo
						.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
								contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
								contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes(),
								contact.getUser(), contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom())));
				return _contact;
			} else {
				contact.setRole(Role.ROLE_PARTENAIRE_CONSULTING);
				Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(),
						contact.getEmail(), contact.getTelephone(), contact.getEntreprise(), contact.getRole(),
						contact.getPays(), contact.getVille(), contact.getAdresse(), contact.getFonction(),
						contact.getNotes(), contact.getUser(), "null"));
				return _contact;

			}
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public Contact postPartenaireUT(Contact contact, long id) {
		try {
			Contact Contact = contactRepo.findById(id).get();
			contact.setRole(Role.ROLE_PARTENAIRE);
			Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
					contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
					contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes()));
			return _contact;
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);

		}
	}

	@Override
	public Contact postCommercialUT(Contact contact, long id) {
		try {

			Contact Contact = contactRepo.findById(id).get();
			contact.setRole(Role.ROLE_COMMERCIAL);
			System.out.println(contact.getRole());
			Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
					contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
					contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes(),
					contact.getUser(), contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom())));
			return _contact;
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);

		}

	}

	@Override
	public Contact postCommercialEducation(Contact contact, long id) {
		try {

			Contact Contact = contactRepo.findById(id).get();
			contact.setRole(Role.ROLE_COMMERCIAL_EDUCATION);
			System.out.println(contact.getRole());
			Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
					contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
					contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes(),
					contact.getUser(), contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom())));
			return _contact;
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);

		}

	}
	@Override
	public Contact postCommercialConsulting(Contact contact, long id) {
		try {

			Contact Contact = contactRepo.findById(id).get();
			contact.setRole(Role.ROLE_COMMERCIAL_CONSULTING);
			System.out.println(contact.getRole());
			Contact _contact = contactRepo.save(new Contact(contact.getNom(), contact.getPrenom(), contact.getEmail(),
					contact.getTelephone(), contact.getEntreprise(), contact.getRole(), contact.getPays(),
					contact.getVille(), contact.getAdresse(), contact.getFonction(), contact.getNotes(),
					contact.getUser(), contact.generateCreatedBy(Contact.getNom(), Contact.getPrenom())));
			return _contact;
		} catch (Exception e) {
			throw new CustomException("Email est déjà utilisé", HttpStatus.UNPROCESSABLE_ENTITY);

		}

	}



	@Override
	public List<Contact> findCommercial() {
		return contactRepo.findCommercial();
	}


	@Override
	public Contact updateUser(long id, Contact user) {
		try {
			System.out.println("Update user with ID..." + id);
			System.out.println(user.getNom() + user.getPrenom() + user.getTelephone() + user.getEmail());
			Optional<Contact> userData = contactRepo.findById(id);
			if (userData.isPresent()) {
				Contact _user = userData.get();
				_user.setNom(user.getNom());
				_user.setPrenom(user.getPrenom());
				_user.setEmail(user.getEmail());
				_user.setTelephone(user.getTelephone());
				_user.setNotes(user.getNotes());
				return contactRepo.save(_user);

			} else {
				return null;
			}
		} catch (Exception e) {
			System.err.println("catch contact 4");
			System.err.println(e.toString());
			System.err.println("catch contact 4");
			return null;
		}
	}

	@Override
	public Contact updateNote(long id, String note) {
		try {
			System.out.println("Update user with ID..." + id);
			Optional<Contact> userData = contactRepo.findById(id);
			if (userData.isPresent()) {
				Contact _user = userData.get();
				_user.setNotes(_user.getNotes() + " " + note);
				return contactRepo.save(_user);
			} else {
				return null;
			}
		} catch (Exception e) {
			System.err.println("catch contact 4");
			System.err.println(e.toString());
			System.err.println("catch contact 4");
			return null;
		}
	}

	@Override
	public Contact update(long id, Contact contact) {
		try {
			System.out.println("Update user with ID..." + id);
			Optional<Contact> nouveauContact = contactRepo.findByEmail(contact.getEmail());
			Optional<Contact> ancienContact = contactRepo.findById(id);

			if (nouveauContact.isPresent() &&  !nouveauContact.get().getEmail().equals(ancienContact.get().getEmail())) {
				System.err.println("email existe dèjà mais pour un contact different");
				return null;

			} else if (nouveauContact.isPresent() && nouveauContact.get().getEmail().equals(ancienContact.get().getEmail())){
				Contact nv_contact = ancienContact.get();
				nv_contact.setNom(contact.getNom());
				nv_contact.setPrenom(contact.getPrenom());
				nv_contact.setTelephone(contact.getTelephone());
				nv_contact.setEntreprise(contact.getEntreprise());
				nv_contact.setRole(contact.getRole());
				nv_contact.setFonction(contact.getFonction());
				nv_contact.setPays(contact.getPays());
				nv_contact.setVille(contact.getVille());
				nv_contact.setAdresse(contact.getAdresse());
				nv_contact.getUser().setRole(contact.getRole());
				return contactRepo.save(nv_contact);
			}
				else if (!nouveauContact.isPresent()){

					Contact nv_contact = ancienContact.get();
					nv_contact.setNom(contact.getNom());
					nv_contact.setPrenom(contact.getPrenom());
					nv_contact.setTelephone(contact.getTelephone());
					nv_contact.setEntreprise(contact.getEntreprise());
					nv_contact.setRole(contact.getRole());
					nv_contact.setFonction(contact.getFonction());
					nv_contact.setPays(contact.getPays());
					nv_contact.setVille(contact.getVille());
					nv_contact.setAdresse(contact.getAdresse());
					nv_contact.setEmail(contact.getEmail());
					nv_contact.getUser().setEmail(contact.getEmail());
					nv_contact.getUser().setRole(contact.getRole());

					return contactRepo.save(nv_contact);

				}
				else {
					return null;
				}

		} catch (Exception e) {
			System.err.println(e.toString());
			return null;
		}
	}

	public Optional<Contact> findByEmail(String email) {
		return contactRepo.findByEmail(email);
	}

	public Contact save(Contact contact) {
		return contactRepo.save(contact);
	}

	@Override
	public Optional<Contact> findById(long id) {
		return contactRepo.findById(id);
	}
	@Override
	public List<Contact> findContactByEntreprise(Entreprise entreprise) {
		return contactRepo.findContactByEntreprise(entreprise);
	}

	@Override
	public Contact saveCommercialFavorisStage(Contact contact) {
		return contactRepo.saveAndFlush(contact);
	}

	@Override
	public Contact saveCommercialFavorisEtudiant(Contact contact) {

		return contactRepo.saveAndFlush(contact);
	}

	@Override
	public Contact updateContactEtudiant(Contact contact) {

		return contactRepo.saveAndFlush(contact);
	}

	@Override
	public Contact updateStatus(long id, Contact contact) {
		try {
			System.out.println("Update contact with ID = " + id + "...");
			Optional<Contact> contactData = findById(id);
			Contact _contact = contact;
			if (contactData.isPresent()) {
				_contact.setStatus(contact.getStatus());

			}
			return save(_contact);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
