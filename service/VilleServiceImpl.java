package elitech.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import elitech.model.Pays;
import elitech.model.Ville;
import elitech.repository.PaysRepo;
import elitech.repository.VilleRepo;

@Service
public class VilleServiceImpl implements VilleService {

	@Autowired
	VilleRepo repository;
	
	@Autowired
	PaysRepo Paysrepository;

	@Override
	public List<Ville> getAll() {
		try {
			//System.out.println("Get all Ville...");
			List<Ville> ville = new ArrayList<>();
			repository.findAll().forEach(ville::add);
			return ville;
		} catch (Exception e) {
			System.err.println("catch Ville 1");
			System.err.println(e.toString());
			System.err.println("catch Ville 1");
			return null;
		}
	}
	@Override
	public List<Ville> getVilleByPays(long id) {
		Optional<Pays> pays=Paysrepository.findById(id);
		if(pays.isPresent()) {
			return repository.findByPays(pays.get());
		}else return null;
		
	}
	

}
