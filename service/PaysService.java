package elitech.service;

import java.util.List;
import java.util.Optional;

import elitech.model.Pays;
import elitech.model.Ville;

public interface PaysService {

	List<Pays> getAll();
	
	Ville save(String pays , String ville , String etat) ;
	
	Optional<Pays> findbyLibelle(String libelle) ;

}
