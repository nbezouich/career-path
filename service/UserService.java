package elitech.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.repository.query.Param;

import elitech.model.Contact;
import elitech.model.User;

public interface UserService {

	String signin(String email, String password);
	String signinmicro(String email);

	String signup(User user);

	String delete(String email);

	String update (User user);
	
	String updatePassword (User user);

	User search(String email);

	User findByUsername(HttpServletRequest req);
	
	public Contact signupWithContact(Contact contact,long id) ;

	String refresh(String email);

	Optional<User> findByEmail(String email); 
	
	String ChangeEtat (String email);
	
	List<Contact> ListContactFavorisByConnectedUser(long id);
	
	User updatefavoris (User user);
}
