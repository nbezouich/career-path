package elitech.service;

import elitech.exception.CustomException;

import elitech.model.Cours;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import elitech.repository.CoursRepository;

import java.util.*;

@Service

public class CoursServiceImpl implements CoursService {

    @Autowired
    CoursRepository repository;

    @Override
    public List<Cours> getAll() {
        try {

            List<Cours> cours = (List<Cours>) repository.findAll();
            return cours;
        } catch (Exception e) {
            System.err.println("catch cours ");
            System.err.println(e.toString());
            System.err.println("catch cours ");
            return null;
        }
    }

    public ArrayList<Cours> getPrerequisites(Cours cours){
        return cours.parsePrerequis();

    }

    @Override
    public Optional<Cours> findById(long id) {
        // TODO Auto-generated method stub
        return repository.findById(id);
    }



    @Override
    public Cours update(long id, Cours cours ) {
        try {
            System.out.println("Update course with ID..." + id);
            Optional<Cours> cours1 = repository.findById(id);
            if (cours1.isPresent()) {
                Cours _cours = cours1.get();
                _cours.setTitle(cours.getTitle());
                _cours.setChapitres(cours.getChapitres());
                _cours.setLink(cours.getLink());
                _cours.setCoursesDependency(cours.getCoursesDependency());

                return repository.save( _cours);
            } else {
                return null;
            }
        } catch (Exception e) {
            System.err.println(e.toString());
            return null;
        }
    }
    @Override
    public Cours postCours(Cours _cours, long id) {
        try {


            Cours cours = repository.save(new Cours(_cours.getId(), _cours.getTitle(), _cours.getChapitres(),_cours.getCoursesDependency()));
            return cours;
        } catch (Exception e) {
            throw new CustomException("Cours déjà enregistré", HttpStatus.UNPROCESSABLE_ENTITY);

        }
    }

    @Override
    public void deleteById(long id) {
        repository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }


}