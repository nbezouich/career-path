package elitech.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import elitech.model.Contact;
import elitech.model.JwtResponse;
import elitech.model.Password;
import elitech.model.Response;
import elitech.model.Role;
import elitech.model.User;
import elitech.service.ContactService;
import elitech.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/users")
@Api(tags = "users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	@PostMapping("/signinmicrosoft")
	@ApiOperation(value = "${UserController.signin}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Quelque chose a mal fait"), //
			@ApiResponse(code = 422, message = "Email incorrect") })
	public ResponseEntity<?> loginmicrosoft(@RequestBody User user) {
		//System.out.print(user.isEtat());
		Contact _contact = contactService.findByEmail(user.getEmail()).get();
		if(_contact.getRole()== Role.ROLE_ADMIN || _contact.getRole()== Role.ROLE_COMMERCIAL || _contact.getRole()== Role.ROLE_ETUDIANT || _contact.getRole()== Role.ROLE_PARTENAIRE 
				|| _contact.getRole()== Role.ROLE_PARTENAIRE_EDUCATION || _contact.getRole()== Role.ROLE_COMMERCIAL_EDUCATION ) {
		String jwt = userService.signinmicro(user.getEmail());
		return ResponseEntity.ok(new JwtResponse(jwt));
		}else
			{
			return null;
			}
	}


	@PostMapping("/signin")
	@ApiOperation(value = "${UserController.signin}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Quelque chose a mal fait"), //
			@ApiResponse(code = 422, message = "Email / Mot de passe incorrect") })
	public ResponseEntity<?> login(@RequestBody User user) {
		//System.out.print(user.isEtat());
		Contact _contact = contactService.findByEmail(user.getEmail()).get();
		if(_contact.getRole()== Role.ROLE_ADMIN || _contact.getRole()== Role.ROLE_COMMERCIAL || _contact.getRole()== Role.ROLE_ETUDIANT || _contact.getRole()== Role.ROLE_PARTENAIRE 
				|| _contact.getRole()== Role.ROLE_PARTENAIRE_EDUCATION || _contact.getRole()== Role.ROLE_COMMERCIAL_EDUCATION ) {
		String jwt = userService.signin(user.getEmail(), user.getPassword());
		return ResponseEntity.ok(new JwtResponse(jwt));
		}else
			{
			return null;
			}
	}

	@PostMapping("/signup/{idconnected}")
	@ApiOperation(value = "${UserController.signup}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Quelque chose a mal fait"), //
			@ApiResponse(code = 403, message = "Accès refusé"), //
			@ApiResponse(code = 422, message = "Email est déjà utilisé"), //
			@ApiResponse(code = 500, message = "Token JWT expiré ou non valide") })
	public ResponseEntity<?> signup(@ApiParam("Signup User") @RequestBody Contact contact,@PathVariable("idconnected") long id) {
		Contact _contact = userService.signupWithContact(contact, id);
		return ResponseEntity.ok(_contact);
	}

	@DeleteMapping(value = "/{username}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@ApiOperation(value = "${UserController.delete}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Quelque chose a mal fait"), //
			@ApiResponse(code = 403, message = "Accès refusé"), //
			@ApiResponse(code = 422, message = "Email est déjà utilisé"), //
			@ApiResponse(code = 500, message = "Token JWT expiré ou non valide") })
	public String delete(@ApiParam("Username") @PathVariable String username) {
		userService.delete(username);
		return username;
	}

	@GetMapping(value = "/{username}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@ApiOperation(value = "${UserController.search}", response = User.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Quelque chose a mal fait"), //
			@ApiResponse(code = 403, message = "Accès refusé"), //
			@ApiResponse(code = 422, message = "Email est déjà utilisé"), //
			@ApiResponse(code = 500, message = "Token JWT expiré ou non valide") })
	public User search(@ApiParam("Username") @PathVariable String username) {
		return userService.search(username);
	}

	@GetMapping(value = "/me")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_COMMERCIAL')")
	@ApiOperation(value = "${UserController.me}", response = User.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Quelque chose a mal fait"), //
			@ApiResponse(code = 403, message = "Accès refusé"), //
			@ApiResponse(code = 422, message = "Email est déjà utilisé"), //
			@ApiResponse(code = 500, message = "Token JWT expiré ou non valide") })
	public User findByUsername(HttpServletRequest req) {
		return userService.findByUsername(req);
	}

	
	@PutMapping("/update")
	public ResponseEntity<?> updateStatus(@RequestBody User user) {
		String msg = userService.update(user);
		return ResponseEntity.ok(new Response(msg));
	}

	@PostMapping("/delete")
	public ResponseEntity<?> deleteUser(@RequestBody String email) {
		try {
			String msg = userService.delete(email);
			return ResponseEntity.ok(new Response(msg));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@PostMapping("/changeEtat")
	public ResponseEntity<?> changeEtat(@RequestBody String email) {
		try {
			String msg = userService.ChangeEtat(email);
			return ResponseEntity.ok(new Response(msg));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@PutMapping("/updatePassword/{email}")
	public ResponseEntity<?> updatePassword(@RequestBody String password,@PathVariable("email") String email) {
		User user = userService.findByEmail(email).get();
		user.setPassword(password);
		//System.out.println(password);
		//System.out.println(email);
		String msg = userService.updatePassword(user);
		return ResponseEntity.ok(new Response(msg));
	}
	
	@PutMapping("/updatePasswordProfil/{email}")
	public ResponseEntity<?> updatePasswordProfil(@RequestBody Password password,@PathVariable("email") String email) {
		User user = userService.findByEmail(email).get();
		String msg;
		if(passwordEncoder.matches(password.getAncien(), user.getPassword()) == true) {
			user.setPassword(password.getNouveau());
			 msg = userService.updatePassword(user);
				return ResponseEntity.ok(new Response(msg));

		}else
		{
			msg = "Verifier votre ancienne mot de passe";
			return null;

		}
		
	}
	
	@PostMapping(value = "/create/favoris/{id}")
	public User ajouterFavorisContact(@RequestBody User user, @PathVariable("id") long id) {
		try {
		//	System.out.println("begin create favoris");
			Set<Contact> listContact= new HashSet<Contact>();
			Contact contact= contactService.findById(id).get();
			User _user = userService.findByEmail(user.getEmail()).get();
			if(_user.getListContact().size() != 0) {
				listContact = _user.getListContact();
				}
	
			listContact.add(contact);
		//	System.out.println("list contact user service "+ listContact.size());

			_user.setListContact(listContact);
			 userService.updatefavoris(_user);
			// System.out.println(listContact);
			 return _user;
			
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@PostMapping(value = "/remove/favoris/{id}")
	public User defavorisContact(@RequestBody User user, @PathVariable("id") long id) {
		try {
			//System.out.println("begin create favoris");
			Set<Contact> listContact= new HashSet<Contact>();
			Contact contact= contactService.findById(id).get();
			User _user = userService.findByEmail(user.getEmail()).get();

				listContact = _user.getListContact();
			
	
			listContact.remove(contact);
			_user.setListContact(listContact);
			 userService.updatefavoris(_user);
			 return _user;
			
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@GetMapping(value = "/favoriscontactbyconnecteduser/{id}")
	public List<Contact> ListContactFavorisByConnectedUser(@PathVariable("id") long id) {
		try {
			List<Contact> listcontact = new ArrayList<>();
			if(userService.ListContactFavorisByConnectedUser(id).isEmpty() == false) {
			return userService.ListContactFavorisByConnectedUser(id);
			}else {
				return listcontact;
			}
						
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@GetMapping(value = "getuser/{username}")
	public User searchbyemail(@ApiParam("Username") @PathVariable String username) {
		return userService.search(username);
	}

/*
	@GetMapping(value = "/image/{id}")
	public @ResponseBody byte[] getImage(@PathVariable("id") long id) throws IOException {
		Contact _contact = contactService.getcontactById(id).get();
		String Path = _contact.getPathImageProfil();
		InputStream in = getClass().getResourceAsStream("/home/gitlab-runner/uploads/resumes/"+Path);
		return IOUtils.toByteArray(in);
	}

	@GetMapping(
			value = "/get-image-with-media-type/{id}",
			produces = MediaType.IMAGE_JPEG_VALUE
	)
	public @ResponseBody byte[] getImageWithMediaType(@PathVariable("id") long id) throws IOException {
		Contact _contact = contactService.getcontactById(id).get();
		String Path = _contact.getPathImageProfil();
		InputStream in = getClass().getResourceAsStream("//home//gitlab-runner//uploads//resumes//"+Path);
		return IOUtils.toByteArray(in);
	}
*/
}

