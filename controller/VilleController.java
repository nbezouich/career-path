package elitech.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import elitech.model.Pays;
import elitech.model.Ville;
import elitech.service.VilleService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class VilleController {

	@Autowired
	VilleService service;

	@GetMapping("/villes")
	public List<Ville> getAllVille() {
		return service.getAll();
	}


	@GetMapping("/villes/pays/{id}")
	public List<Ville> getAllVilleByPays(@PathVariable("id") long id) {
		return service.getVilleByPays(id);
	}
}
