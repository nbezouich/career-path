package elitech.controller;

import elitech.model.Cours;
import elitech.repository.CoursRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import elitech.service.CoursServiceImpl;


import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/course")
public class CoursController {

    @Autowired
    private final CoursServiceImpl coursService;
    @Autowired
    private final CoursRepository coursRepository;
    public CoursController(CoursServiceImpl coursService, CoursRepository coursRepository) {
        this.coursService = coursService;
        this.coursRepository = coursRepository;

    }

    @GetMapping("/")
    public void save(){
        coursRepository.saveAll();
    }


    @GetMapping("/all")
   public Iterable<Cours>findAll() {


         return coursService.getAll();

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<Cours> getCourse(@PathVariable long id){
        return coursService.findById(id);


    }

    @PostMapping(value = "{id}/create")
    public Cours postCandidatUT(@RequestBody Cours cours, @PathVariable("id") long id) {
        return coursService.postCours(cours, id);
    }

    @GetMapping ("/{id}/prerequisites")
        public ArrayList<Cours> prerequisites(@PathVariable long id){
            return coursService.getPrerequisites(coursRepository.findById(id).orElseGet(null));
        }




    @RequestMapping(value = "deleteAll/", method = RequestMethod.GET)

    public void deleteCourse()
    {
        coursService.deleteAll();
    }



    @RequestMapping(value = "{id}/delete", method = RequestMethod.GET)

    public void deleteCourse(@PathVariable long id)
    {
        coursService.deleteById(id);
    }


    @PutMapping("{id}/update")
    public ResponseEntity<Cours> updateCourse(@RequestBody Cours cours, @PathVariable long id) {

        try {
            Cours updated = coursService.update(id, cours);
            return new ResponseEntity<>(updated, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }







}