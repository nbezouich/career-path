package elitech.controller;

import java.sql.Blob;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import elitech.dto.ContactDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import elitech.model.Contact;
import elitech.model.Entreprise;
import elitech.repository.ContactRepo;
import elitech.service.ContactService;
import elitech.service.EntrepriseService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ContactController {

	@Autowired
	ContactService contactservice;

	@Autowired
	EntrepriseService entrepriseService;

	@PostMapping(value = "/contacts/candidat/create/{id}")
	public Contact postCandidatUT(@RequestBody Contact contact, @PathVariable("id") long id) {
		return contactservice.postCandidatUT(contact, id);
	}

	@PostMapping(value = "/contacts/partenaire/create/{id}")
	public Contact postPartenaireUT(@RequestBody Contact contact, @PathVariable("id") long id) {
		return contactservice.postPartenaireUT(contact, id);
	}




	@GetMapping(value = "/contacts/commercial")
	public List<Contact> findCommercial() {
		return contactservice.findCommercial();
	}



	@GetMapping(value = "/contacts/contactsByEntreprise/{id}")
	public List<Contact> getContactEntreprise(@PathVariable("id") long id) {
		Entreprise entreprise = entrepriseService.findById(id).get();
		if (entreprise != null) {
			return contactservice.findContactByEntreprise(entreprise);
		}
		return null;
	}

	@GetMapping(value = "/contact/{email}")
	public Contact findConnectedUser(@PathVariable("email") String email) {
	//	System.out.print(email);
		Optional<Contact> contact = contactservice.findByEmail(email);
		if (contact.isPresent()) {
			return contact.get();
		} else
			return null;

	}




	@PutMapping("/contacts/updateNote/{id}")
	public ResponseEntity<Contact> updateCandidat(@PathVariable("id") long id, @RequestBody String note) {
		try {
			Contact updated = contactservice.updateNote(id, note);
			return new ResponseEntity<>(updated, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/contacts/update/{id}")
	public ResponseEntity<Contact> update(@PathVariable("id") long id, @RequestBody Contact contact) {
		try {
			Contact updated = contactservice.update(id, contact);
			return new ResponseEntity<>(updated, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@GetMapping("/contacts/findContactByid/{id}")
	public Contact getcontactById(@PathVariable("id") long id) {
		
		return contactservice.getcontactById(id).get();
		
	}




	/*@GetMapping(value = "/contact/contactFavoris/{id}")
	public List<Contact> getContactEtudiantfavorisByConnectedUser(@PathVariable("id") Long id) {
		if (contactservice.getContactEtudiantfavorisByConnectedUser(id).size() != 0) {
		//	System.out.println("favoris contact " + contactservice.getContactEtudiantfavorisByConnectedUser(id).size());
			return contactservice.getContactEtudiantfavorisByConnectedUser(id);
		} else {
			return null;
		}

	}*/
	

	@PutMapping("/contacts/status/{id}")
	public ResponseEntity<Contact> updateStatus(@PathVariable("id") long id, @RequestBody Contact contact) {
		try {
			Contact updated = contactservice.updateStatus(id, contact);
			return new ResponseEntity<>(updated, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(contact, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
