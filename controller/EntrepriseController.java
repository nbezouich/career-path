package elitech.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import elitech.model.Contact;
import elitech.model.Entreprise;
import elitech.service.ContactService;
import elitech.service.EntrepriseService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class EntrepriseController {

	@Autowired
	EntrepriseService service;
	
	@Autowired
	ContactService contactservice;

	@GetMapping("/entreprises")
	public List<Entreprise> getAllEntreprise() {
		List<Entreprise> entreprises = new ArrayList<>();
		entreprises = service.getAll();
		entreprises.stream().sorted(Comparator.comparing(Entreprise::getDate, Comparator.reverseOrder()));
		return entreprises;
	}

	@PostMapping(value = "/entreprises/create/{id}")
	public Entreprise postEntreprise(@PathVariable("id") long id, @RequestBody Entreprise entreprise) {
		Optional<Contact> Contact = contactservice.findById(id);
		if (Contact.isPresent()) {
		return service.save(new Entreprise(entreprise.getLibelle() ,entreprise.getDate(),entreprise.generateCreatedBy(Contact.get().getNom(), Contact.get().getPrenom())
				,entreprise.getIdUnique(), entreprise.getAdresse(), entreprise.getTel() , entreprise.getDescription(),entreprise.getEmail(),entreprise.getLien()));
			}else {
		return null;
		}
	
	}
	
	@GetMapping("/entreprises/elitech")
	public List<Entreprise> getAllEntrepriseElitech() {
		List<Entreprise> entreprises = new ArrayList<>();
		entreprises = service.getListElitech();
		entreprises.sort(Comparator.comparing(Entreprise::getLibelle));
		return entreprises;
	}
	
	@GetMapping("/entreprises/{id}")
	public Entreprise getEntreprise(@PathVariable("id") long id) {
				return service.findById(id).get();
	}
	
	@PutMapping("/entreprises/update/{id}")
	public ResponseEntity<Entreprise> update(@PathVariable("id") long id, @RequestBody Entreprise entreprise) {
		try {
			Entreprise updated = service.update(id, entreprise);
			return new ResponseEntity<>(updated, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
