package elitech.controller;

import elitech.model.Cours;
import elitech.model.Parcours;
import elitech.repository.ParcoursRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/parcours")
public class ParcoursController {
    @Autowired
    private final ParcoursRepository parcoursRepository;


    public ParcoursController(ParcoursRepository parcoursRepository) {
        this.parcoursRepository = parcoursRepository;
    }
    @GetMapping("/")
    public void save() {

         parcoursRepository.saveParcours();
    }


    @GetMapping("/all")
    public Iterable<Parcours> findAll() {

        return parcoursRepository.findAll();
    }
    @GetMapping("{id}/prerequis")
    public void saveRelationship() {

         parcoursRepository.saveRelationship();
    }
}
