package elitech.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import elitech.exception.CustomException;
import elitech.model.Pays;
import elitech.model.PaysVilleDto;
import elitech.model.Response;
import elitech.model.Ville;
import elitech.service.PaysService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PaysController {

	@Autowired
	PaysService service;

	@GetMapping("/pays")
	public List<Pays> getAllPays() {
		List<Pays> pays = new ArrayList<>();
		pays = service.getAll();
		pays.sort(Comparator.comparing(Pays::getLibelle));
		return pays;
	}
	
	@PostMapping(value = "/pays/create")
	public Ville postPays(@RequestBody PaysVilleDto paysvilledto) {
		System.out.print(" ville =" +paysvilledto.getLibelle()+ " pays =" + paysvilledto.getPays()+" etat =" + paysvilledto.getEquivalents());
		return service.save(paysvilledto.getPays(), paysvilledto.getLibelle() , paysvilledto.getEquivalents());

	}
	
	@GetMapping("/pays/{libelle}")
	public Optional<Pays> getPays( @PathVariable("libelle") String libelle) {
		return service.findbyLibelle(libelle);
	}
	

}
