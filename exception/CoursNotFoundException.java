package elitech.exception;

public class CoursNotFoundException extends RuntimeException {

    public CoursNotFoundException(String exception) {
        super(exception);
    }

}