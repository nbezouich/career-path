package elitech;

import elitech.repository.CoursRepository;
import elitech.service.ContactService;
import elitech.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
@EnableTransactionManagement
@EnableNeo4jRepositories
@SpringBootApplication

public class CareerPathApp {
    private final static Logger log = LoggerFactory.getLogger(CareerPathApp.class);


    public static void main(String[] args) {



    SpringApplication.run(CareerPathApp.class, args);
}

@Bean
public RestTemplate getRestTemplate() {


    return new RestTemplate();
}

}
class DefaultRunner implements CommandLineRunner {


    @Autowired
    private Environment environment;

   @Override
   public void run(String... args) throws Exception {
       System.out.println("Active profiles: " +
               Arrays.toString(environment.getActiveProfiles()));


   }
}


@Component
@Profile(value = "dev")
class DevRunner implements CommandLineRunner {

    @Autowired
    UserService userService;

    @Autowired
    ContactService contactService;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("In DEV SERVER");

    }
}

@Component
@Profile(value = "prod")
class ProdRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

        System.out.println("In production");
    }
}

@Component
@Profile(value="local")
class LocalRunner implements CommandLineRunner {
    @Autowired
    UserService userService;

    @Autowired
    ContactService contactService;
    @Autowired
    CoursRepository coursRepository;

    @Override
    public void run(String... args) throws Exception {

        coursRepository.saveAll();

       /* System.out.println("In local");
        User admin = new User();
        admin.setEmail("admin");
        admin.setPassword("admin");
        admin.setRole(Role.ROLE_ADMIN);
        admin.setEtat(true);
        Contact adminContact = new Contact();
        adminContact.setEmail("admin");
        adminContact.setNom("user");
        adminContact.setPrenom("admin");
        adminContact.setTelephone("22558855");
        adminContact.setRole(Role.ROLE_ADMIN);
        adminContact.setUser(admin);
        adminContact.setCreatedBy("user admin");
        adminContact.setCreatedDate(new Date());
        userService.signup(admin);

        contactService.save(adminContact);

        User commercial = new User();
        commercial.setEmail("commercial");
        commercial.setPassword("commercial");
        commercial.setRole(Role.ROLE_COMMERCIAL);
        commercial.setEtat(true);

        Contact commercialContact = new Contact();
        commercialContact.setEmail("commercial");
        commercialContact.setNom("user");
        commercialContact.setPrenom("commercial");
        commercialContact.setTelephone("22558855");
        commercialContact.setRole(Role.ROLE_COMMERCIAL);
        commercialContact.setUser(commercial);
        commercialContact.setCreatedBy("user admin");
        commercialContact.setCreatedDate(new Date());

        userService.signup(commercial);
        contactService.save(commercialContact);*/
    }

}
@Component
@Profile(value={"dev", "prod"})
class DevProdMsg implements CommandLineRunner {

    @Value("${message}")
    private String message;

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Message: " + message);
    }

}
