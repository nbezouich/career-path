package elitech.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "pays")
public class Pays implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "code")
	private String code;

	@Column(name = "libelle")
	private String libelle;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pays")
	private List<Ville> villes;

	public Pays() {
		super();
	}

	public Pays(Long id) {
		super();
		this.id = id;
	}

	public Pays(String code, String libelle, List<Ville> villes) {
		super();
		this.code = code;
		this.libelle = libelle;
		this.villes = villes;
	}

	public Pays(String code, String libelle) {
		super();
		this.code = code;
		this.libelle = libelle;
	}

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Ville> getVilles() {
		villes.sort(Comparator.comparing(Ville::getLibelle));
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

}