package elitech.model;

import java.io.Serializable;
import java.sql.Blob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "contact")
public class Contact implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nom")
	private String nom;

	@Column(name = "prenom")
	private String prenom;

	@Column(unique = true, name = "email")
	private String email;

	@Column(name = "telephone")
	private String telephone;

	@Column(name = "adresse")
	private String adresse;

	@Lob
	@Column(name = "notes", columnDefinition = "TEXT")
	private String notes;

	@Column(name = "fonction")
	private String fonction;

	@Column(name = "role")
	private Role role;

	@ManyToOne
	@JoinColumn(name = "id_entreprise")
	private Entreprise entreprise;

	@ManyToOne
	@JoinColumn(name = "id_pays")
	private Pays pays;

	@ManyToOne
	@JoinColumn(name = "id_ville")
	private Ville ville;

	@OneToOne
	@JoinColumn(name = "id_user")
	private User user;

	@JoinColumn(name = "created_by")
	private String createdBy;

	@JoinColumn(name = "created_date")
	private Date createdDate;

	@Column(name = "idUnique")
	private String idUnique;

	@Column(name = "status")
	private String status;

	public Contact() {
	}
	public Contact(String nom, String email, String telephone, Role role, Entreprise entreprise, String adresse,String notes) {
		super();
		this.nom = nom;
		this.email = email;
		this.telephone = telephone;
		this.role = role;
		this.entreprise = entreprise;
		this.adresse = adresse;
		this.notes = notes;
		this.createdDate = new Date();
		this.status="enabled";
	}
	public Contact(String nom, String email, String telephone, Role role, Entreprise entreprise, String adresse,String notes , User user) {
		super();
		this.nom = nom;
		this.email = email;
		this.telephone = telephone;
		this.role = role;
		this.entreprise = entreprise;
		this.adresse = adresse;
		this.notes = notes;
		this.createdDate = new Date();
		this.user=user;
		this.status="enabled";
	}

	public Contact(String nom, String prenom, String email, String telephone, Entreprise entreprise, Role role,
			Pays pays, Ville ville, String adresse, String fonction, String notes) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.entreprise = entreprise;
		this.role = role;
		this.pays = pays;
		this.ville = ville;
		this.adresse = adresse;
		this.fonction = fonction;
		this.notes = notes;
		this.status="enabled";
	}

	public Contact(String nom, String prenom, String email, String telephone, Entreprise entreprise, Role role,
			Pays pays, Ville ville, String adresse, String fonction, String notes, User user, String createdBy,
			Long idContactAjouter) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.entreprise = entreprise;
		this.role = role;
		this.pays = pays;
		this.ville = ville;
		this.adresse = adresse;
		this.fonction = fonction;
		this.notes = notes;
		this.user = user;
		this.createdBy = createdBy;
		this.createdDate = new Date();
		this.status="enabled";

	}

	public Contact(String nom, String prenom, String email, String telephone, Entreprise entreprise, Role role,
				   Pays pays, Ville ville, String adresse, String fonction, String notes, User user, String createdBy) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.entreprise = entreprise;
		this.role = role;
		this.pays = pays;
		this.ville = ville;
		this.adresse = adresse;
		this.fonction = fonction;
		this.notes = notes;
		this.user = user;
		this.createdBy = createdBy;
		this.createdDate = new Date();
		this.status="enabled";

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Long getId() {
		return id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	@Override
	public String toString() {
		return "nom: " + nom + "prenom: " + prenom;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (id != other.id)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String generateCreatedBy(String nom, String prenom) {
		return nom + " " + prenom;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}





	public String generateIdUnique() {
		DateFormat dateFormat = new SimpleDateFormat("yyMM");
		Date date = new Date();
		if(this.role.getAuthority()=="ROLE_ADMIN"){
			 this.idUnique = "AD_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		else if(this.role.getAuthority()=="ROLE_COMMERCIAL"){
			 this.idUnique = "CG_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		else if(this.role.getAuthority()=="ROLE_PARTENAIRE"){
			 this.idUnique = "PG_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		else if(this.role.getAuthority()=="ROLE_COMMERCIAL_EDUCATION"){
			 this.idUnique = "CE_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		else if(this.role.getAuthority()=="ROLE_COMMERCIAL_CONSULTING"){
			 this.idUnique = "CC_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		else if(this.role.getAuthority()=="ROLE_PARTENAIRE_EDUCATION"){
			 this.idUnique = "PE_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		else if(this.role.getAuthority()=="ROLE_PARTENAIRE_CONSULTING"){
			 this.idUnique = "PC_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		else if(this.role.getAuthority()=="ROLE_CANDIDAT"){
			 this.idUnique = "C_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		else if(this.role.getAuthority()=="ROLE_ETUDIANT"){
			 this.idUnique = "E_" + prenom.substring(0, 1) + nom.substring(0,2)+ "_" + dateFormat.format(date) + "_" + this.id;
		}
		return this.idUnique;

	}

	public String getIdUnique() {
		return idUnique;
	}

	public void setIdUnique(String idUnique) {
		this.idUnique = idUnique;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
}
