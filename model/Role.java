package elitech.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
	ROLE_ADMIN, ROLE_COMMERCIAL, ROLE_CANDIDAT, ROLE_PARTENAIRE , ROLE_ETUDIANT,ROLE_COMMERCIAL_EDUCATION,ROLE_COMMERCIAL_CONSULTING,
	ROLE_PARTENAIRE_EDUCATION,ROLE_PARTENAIRE_CONSULTING;

	@Override
	public String getAuthority() {
		return name();
	}

}