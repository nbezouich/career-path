package elitech.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Size(min = 4, max = 255, message = "Minimum username length: 4 characters")
	@Column(unique = true, nullable = false)
	private String email;

	@Size(min = 8, message = "Minimum password length: 8 characters")
	private String password;

	private boolean etat = true;

	private Role role;

	
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST })
	@JoinTable(name = "UserContact_Favoris", joinColumns = @JoinColumn(name = "idUser", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "idcontact", referencedColumnName = "id"))
	private Set<Contact> listContact = new HashSet<Contact>();

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(@Size(min = 4, max = 255, message = "Minimum username length: 4 characters") String email,
			@Size(min = 8, message = "Minimum password length: 8 characters") String password, Role role,
			boolean etat) {
		super();
		this.email = email;
		this.password = password;
		this.role = role;
		this.etat = etat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role roles) {
		this.role = roles;
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public Set<Contact> getListContact() {
		return listContact;
	}

	public void setListContact(Set<Contact> listContact) {
		this.listContact = listContact;
	}

}
