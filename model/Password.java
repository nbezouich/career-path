package elitech.model;

import java.io.Serializable;


public class Password implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String ancien;
	private String nouveau;
	
	public String getAncien() {
		return ancien;
	}
	public void setAncien(String ancien) {
		this.ancien = ancien;
	}
	public String getNouveau() {
		return nouveau;
	}
	public void setNouveau(String nouveau) {
		this.nouveau = nouveau;
	}
	
	
}
