package elitech.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "entreprise")
public class Entreprise implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_entreprise")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "libelle")
	private String libelle;

	@Column(name = "date")
	private Date date = new Date();

	@JoinColumn(name = "created_by")
	private String createdBy;
	
	
	@Column(name = "idUnique")
	private String idUnique;
	
	@Column(name = "adresse")
	private String adresse;
	
	@Column(name = "telephone")
	private String tel;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "lien")
	private String lien;
	
	@Column(name = "mail")
	private String email;
	
	
	

	public Entreprise() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Entreprise(String libelle, Date date, String createdBy) {
		super();
		this.libelle = libelle;
		this.date = date;
		this.createdBy = createdBy;
	}

	public Entreprise(String libelle, Date date, String createdBy,  String idUnique, String adresse, String tel, String description,
			String email, String lien
			) {
		super();
		this.libelle = libelle;
		this.date = date;
		this.createdBy = createdBy;
		this.idUnique=idUnique;
		this.adresse=adresse;
		this.tel=tel;
		this.description=description;
		this.email=email;
		this.lien=lien;
			}
	public Entreprise(String libelle, Date date, String createdBy, String adresse, String tel, String description,
					  String email, String lien
	) {
		super();
		this.libelle = libelle;
		this.date = date;
		this.createdBy = createdBy;
		this.adresse=adresse;
		this.tel=tel;
		this.description=description;
		this.email=email;
		this.lien=lien;
	}
	
	public String generateIdUnique() {
		DateFormat dateFormat = new SimpleDateFormat("yyMM");
		Date date = new Date();
		return this.idUnique = "T_" + dateFormat.format(date)  + "_" + this.id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Long getId() {
		return id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String generateCreatedBy(String nom , String prenom) {
		return nom + " "  + prenom;
}

	public String getIdUnique() {
		return idUnique;
	}

	public void setIdUnique(String idUnique) {
		this.idUnique = idUnique;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLien() {
		return lien;
	}

	public void setLien(String lien) {
		this.lien = lien;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
}
