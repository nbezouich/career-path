package elitech.model;

public class PaysVilleDto {

	private String pays;

	private String libelle;
	
	private String equivalents;

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getEquivalents() {
		return equivalents;
	}

	public void setEquivalents(String equivalents) {
		this.equivalents = equivalents;
	}
	
	
}
