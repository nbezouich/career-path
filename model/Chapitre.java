package elitech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@NodeEntity
public class Chapitre {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String contenu;

    @JsonIgnoreProperties("chapitre")



    public Chapitre() {

    }

    public  Chapitre(Long id,String name) {
        this.id=id;
        this.title = name;

    }

    public Long getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }
    public String getContenu() {
        return contenu;
    }


}