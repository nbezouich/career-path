package elitech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Cours {

    @Id

    @GeneratedValue

    private Long id;
    private String title;
    private String link;
    private String description;



    @JsonIgnoreProperties("cours")

    @Relationship(type = "APPARTIENT", direction = Relationship.INCOMING)

    private Set<Chapitre> chapitres;

    @Relationship(type = "DependsOn", direction = Relationship.OUTGOING)
    private ArrayList<Cours> coursesDependency=new ArrayList<>();
    private ArrayList<Cours> unitsDependency=new ArrayList<>();



    public ArrayList<Cours> parsePrerequis() throws JSONException {

        String desc=description.substring(description.indexOf("{"));
        final JSONObject obj = new JSONObject(desc);
        final JSONArray prerequisites = obj.getJSONArray("prerequisites");
        final int n=prerequisites.length();

        for (int i = 0; i < n; ++i) {

            final JSONObject objet = prerequisites.getJSONObject(i);
            if(objet.getString("type").contains("cours")){
            Cours c = new Cours(objet.getLong("id"),objet.getString("title"),objet.getString("link"));
                coursesDependency.add(c);}

        }


        return coursesDependency;


    }

    public Cours()
    {
    }

    public Cours(Long id, String title, String link) {
        this.id = id;
        this.title = title;
        this.link=link;
    }

    public Cours(Long id, String title, Set<Chapitre> chapitres,ArrayList<Cours> coursesDependency) {
        this.id =id;
        this.title =title;
        this.link=link;
        this.chapitres=chapitres;
        this.coursesDependency=coursesDependency;

    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
    public String getLink() {
        return link;
    }


    public HashSet<Chapitre> getChapitres() {
        return (HashSet<Chapitre>) chapitres;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }


    public void setChapitres(Set<Chapitre> chapitres) {
        this.chapitres = chapitres;
    }


    public ArrayList<Cours> getCoursesDependency() {
        return coursesDependency;
    }

    public void setCoursesDependency(ArrayList<Cours> coursesDependency) {
        this.coursesDependency = coursesDependency;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}





