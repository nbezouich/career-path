package elitech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.web.client.RestTemplate;


import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Parcours {

    @Id

    @GeneratedValue

    private Long id;
    private String title;


    @JsonIgnoreProperties("parcours")

    @Relationship(type = "Require", direction = Relationship.OUTGOING)

    private Set<Cours> courses;






    /*public void appartient(Chapitre chapitre) {
        if (chapitres == null) {
            chapitres = new HashSet<Chapitre>() {
            };
        }
        chapitres.add(chapitre);
    }


    public void depend(Cours cours) {
        if (coursesDependency == null) {
            coursesDependency = new HashSet<Cours>() {
            };
        }
        coursesDependency.add(cours);
    }*/

    public Parcours() {
    }

    public Parcours(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    /*public String[] getCoursFromApi(){
        final String uri = "http://dev-training.elitech.fr/wp-json/wplms/v1/course";

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);
        String[] coursFromApi= result.split("data");

        System.out.println(coursFromApi[1]);
        return  coursFromApi;}*/

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public HashSet<Cours> getCourses() {
        return (HashSet<Cours>) courses;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCourses(Set<Cours> courses) {
        this.courses = courses;
    }


}