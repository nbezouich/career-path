package elitech.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ville")
public class Ville implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "code")
	private String code;

	@Column(name = "libelle")
	private String libelle;

	@Column(name = "equivalents")
	private String equivalents;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_pays")
	private Pays pays;

	public Ville() {
		super();
	}

	public Ville(String code, String libelle, String equivalents, Pays pays) {
		super();
		this.code = code;
		this.libelle = libelle;
		this.equivalents = equivalents;
		this.pays = pays;
	}

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

	public String getEquivalents() {
		return equivalents;
	}

	public void setEquivalents(String equivalents) {
		this.equivalents = equivalents;
	}
}